class PlateChecker
	
	def initialize(plate_number, date, time)
		@plate_number = plate_number
		@date = date
		@time = time
	end


	#= Defining getters
	def plate_number
		@plate_number
	end

	def date
		@date
	end

	def time
		@time
	end

	def check_plate_number
		require 'date'
		begin
		  selected_day = Date.parse(@date).wday
		rescue => e
			forbidden = nil
			message = "There's been a problem with your date. Please make sure it uses the format dd-mm-yyyy"
			return message, forbidden
		end

		week_days = ["Monday", "Tuesday", "Wednesday", "Thrusday", "Friday"] # Weekdays
		selected_day_name = week_days[selected_day - 1]
		forbidden = nil
		last_digit = ""
		last_digit = @plate_number[-1] # Getting last number

		

		# Check if car is allowed to go out or not
		forbidden = case last_digit
		when "1", "2"
			selected_day_name == week_days[0]
		when "3", "4"
			selected_day_name == week_days[1]
		when "5", "6"
			selected_day_name == week_days[2]
		when "7", "8"
			selected_day_name == week_days[3]
		when "9", "0"
			selected_day_name == week_days[4]
		end

		if forbidden
			if(self.get_complete_datetime > DateTime.parse("#{self.date} 07:00 AM") && self.get_complete_datetime < DateTime.parse("#{self.date} 09:30 AM") || self.get_complete_datetime > DateTime.parse("#{self.date} 04:00 PM") && self.get_complete_datetime < DateTime.parse("#{self.date} 07:30 PM"))
				# Do nothing here
			else
				forbidden = false
			end
		end
		
		if forbidden
			message = "Your car is not allowed go out at this time of the day :-("
		else
			message = "You can go out for a ride :-)"
		end

		

		return message, forbidden
			
	end	


	def get_complete_datetime
		DateTime.parse("#{@date} #{@time}") rescue nil
	end


end