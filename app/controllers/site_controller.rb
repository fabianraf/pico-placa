class SiteController  < ApplicationController
include ApplicationHelper
  def index
  	if params.present? && params["data"].present? && params["data"]["plate_number"].present? && params["data"]["time"].present?
  		plate_number = PlateChecker.new(params["data"]["plate_number"], params["data"]["date"], params["data"]["time"])
  		@message = plate_number.check_plate_number # Checking this with helper
  	end
  end
end
