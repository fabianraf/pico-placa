require "spec_helper"

describe PlateChecker do
  
  describe "#check_plate_number" do
    
    context "When plate number, date and time are entered" do
      
       it "Forbids a car from going out on Wednesday before 9 AM" do
        date = "07-06-2017" # For sure this is a Wednesday
        plate_checker = PlateChecker.new("PBU-3216", date, "08:45 AM")
        expect(plate_checker.check_plate_number[1]).to eq(true)
      end


      it "Forbids a car from going out on Wednesday before 8 PM" do
        date = "07-06-2017" # For sure this is a Wednesday
        plate_checker = PlateChecker.new("PBU-3216", date, "06:45 PM")
        expect(plate_checker.check_plate_number[1]).to eq(true)
      end


      it "allows a car to go out on Wednesday in a different time" do
        date = "07-06-2017" # For sure this is a Wednesday
        plate_checker = PlateChecker.new("PBU-3216", date, "10:45 AM")
        expect(plate_checker.check_plate_number[1]).to eq(false)
      end

       it "confirms if a car can go out on Wednesday" do
        date = "07-06-2017" # For sure this is a Wednesday
        plate_checker = PlateChecker.new("PBU-3211", date, "08:45 AM")
        expect(plate_checker.check_plate_number[1]).to eq(false)
      end


      it "should reject a bad date" do
        date = "17-66-2017" # For sure this is a Wednesday
        plate_checker = PlateChecker.new("PBU-3211", date, "08:45 AM")
        expect(plate_checker.check_plate_number[1]).to eq(nil)
      end

      
    end

    
  end
  
  
end


